import {BrowserRouter,Route,Routes} from 'react-router-dom'
import './App.css';
// import RegistrationPage from './Components/RegistrationPage/RegistrationPage';
import LandingPage from './Components/LandingPage/LandingPage';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { solid, regular, brands, icon } from '@fortawesome/fontawesome-svg-core/import.macro';
import FilterSection from './Components/FilterSection'
import Booking from './Components/Booking'
import OffersPage from './Components/OffersPage/OffersPage';

function App() {
  return (
    <div>
      {/* <RegistrationPage/> */}
      {/* <LandingPage/> */}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="flights" element={<FilterSection />} />
          <Route path="booking" element={<Booking />} />
        </Routes>
      </BrowserRouter> 
    </div>
  );
}

export default App;
