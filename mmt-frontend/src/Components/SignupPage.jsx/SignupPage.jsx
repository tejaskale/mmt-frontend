import React, { Fragment } from "react";

function SignupPage() {
  return (
    <Fragment>
      <div className="name-container">
        <div className="name-text-container">
          <p>Username</p>
        </div>
        <input type="text" className="inp-box" placeholder="Username" />
      </div>
      <div>
        <p>Email</p>
        <input type="text" className="inp-box" placeholder="Email" />
      </div>
      <div>
        <p>Phone Number</p>
        <input type="text" className="inp-box" placeholder="Phone Number" />
      </div>
      <div>
        <p>
          Date Of Birth
        </p>
        <input type="date" className="inp-box" />
      </div>

      <div>
        <p>
          Gender
        </p>
        <div className="gender-container">
          <div>        
            <label htmlFor="male">Male</label>
            <input type="radio" value="Male" id="male" name="gender" />
          </div>
          <div>
            <label htmlFor="female">Female</label>
            <input type="radio" value="Female" id="female" name="gender" />
          </div>
          <div>
            <label htmlFor="other">Other</label>
            <input type="radio" value="Other" id="other" name="gender" />
          </div>
        </div>
      </div>

      <div>
        <p>Password</p>
        <input type="password" className="inp-box" placeholder="Password" />
      </div>
      
      <div className="submit-btn-container">
        <button className="submit-btn">
          <h3>Continue</h3>
        </button>
      </div>

    </Fragment>
  );
}

export default SignupPage;
