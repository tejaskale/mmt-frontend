import { useNavigate } from "react-router-dom"
import { useEffect, useState } from "react"
import {TailSpin} from 'react-loader-spinner'
import {BsArrowUp} from 'react-icons/bs'
import FlightDetails from "../FlightDetails"

import './filtersection.css'

const airlinesData = [
    {
        "id": 1,
        "airlineId": "Air India",
        "airlineName":"Air India"
    },
    {
        "id": 2,
        "airlineId": "AirAsia India",
        "airlineName":"AirAsia India"
    },
    {
        "id": 3,
        "airlineId": "Go First",
        "airlineName":"Go First"
    },
    {
        "id": 4,
        "airlineId": "IndiGo",
        "airlineName":"Indigo"
    },
    {
        "id": 5,
        "airlineId": "Qatar Airways",
        "airlineName": "Qatar Airways"
    },
    {
        "id": 6,
        "airlineId": "SpiceJet",
        "airlineName":"SpiceJet"
    }
]

const apiStatusConstants = {
    initial: "INITIAL",
    failure:'FAILURE',
    success:"SUCCESS",
    inProgress:"IN_POGRESS"
}


const FilterSection  = () => {
    const [apiStatus,setApiStatus] = useState(apiStatusConstants.initial)
    const [fromCity,setFromCity] = useState("")
    const [toCity, setToCity] = useState("")
    const [airlineId,setAirlineId] = useState("")
    const [search, setSearch] = useState(false)
    const [flights, setFlightsData] = useState([])

    const formatData = (data) => {
        return ({
            flightId: data.Flight_ID,
            airlineName: data.Airline_name,
            arrivalTime: data.Arrival_time,
            departureTime: data.Departure_time,
            fromCity: data.From_City,
            toCity: data.To_City,
            baggage: data.Baggage,
            price: data.Price,
            airlineImageUrl:data.image_url
        })
    }

    useEffect(()=>{
        const apiUrl = `http://localhost:5000/api/flights/?search=${airlineId}&from_city=${fromCity}&to_city=${toCity}`
        const getFlightsData = async () => {
            const response = await fetch(apiUrl)
           
            if (response.ok) {
                const flightsData = await response.json()
                console.log(response)
                const formattedFlightsData = flightsData.map((flightsDetails)=>(formatData(flightsDetails)))
                setFlightsData(formattedFlightsData)
                setApiStatus(apiStatusConstants.success)
                console.log(formattedFlightsData)
            } else {
                setApiStatus(apiStatusConstants.failure)
            }
        }

        if (search) {
            setApiStatus(apiStatusConstants.inProgress)
            getFlightsData()
            setSearch(false)
        }

    },[fromCity,toCity,airlineId,search,flights,apiStatus])

    const navigate = useNavigate()
    const handleBooking = () => {
        console.log("Initialised the Boooking")
        navigate('/booking')
    }

    const handleSearch = () => {
        setSearch(true)
    }

    const handleClearFilters = () => {    
        setAirlineId("")
        setFlightsData(flights)
        setSearch(true)
    }

    const handleFareType = () => {
        console.log("Fare Type")
    }

    const handleAirlineId = (airlineId) => {
        setAirlineId(airlineId)
        setSearch(prev=>!prev)
    }

    const renderFailureView = () => (
        <div className="failure-view">
            <h1 className="failure-heading">Ooops, Something went wrong.</h1>
        </div>
    )

    const renderSuccessView = () => {
        return flights.map((flight)=> (<FlightDetails key={flight.flightId} flightDetails = {flight} handleBooking={handleBooking} />))
    }

    const renderLoader = () => (
        <div className="loader">
            <TailSpin width="50px" height="50px" color="#344ceb" />
        </div>
    )

    const renderFlights = () => {
        switch(apiStatus) {
            case apiStatusConstants.inProgress:
                return renderLoader()
            case apiStatusConstants.failure:
                return renderFailureView()
            case apiStatusConstants.success:
                return renderSuccessView()
            default:
                return null
        }
    }

    return (
        <div className="main-container">
            <div className="filter-section">
                <div className="filters">
                    <div className="filter-cards">
                        <p className="filter-card-text">TRIP TYPE</p>
                        <select className="select-element">
                            <option>Round Trip</option>
                            <option>Multi City</option>
                        </select>
                    </div>
                    <div className="filter-cards">
                        <p className="filter-card-text">FROM</p>
                        <input type="text" onChange={(event)=>setFromCity(event.target.value)} value={fromCity} className="input-element" placeholder="From" />
                    </div>
                    <div className="filter-cards">
                        <p className="filter-card-text">TO</p>
                        <input type="text" onChange={(event)=>setToCity(event.target.value)} value={toCity} className="input-element" placeholder="To" />
                    </div>
                    <div className="filter-cards">
                        <p className="filter-card-text">DEPART</p>
                        <input type='date' className="input-element" />
                    </div>
                    <button type="button" className="search-button active" onClick={handleSearch}>Search</button>
                </div>
                <div className="fare-type-container">
                    <p className="fare-type">Fare Type:</p>
                    <div className="fare-type-items">
                        <div className="radio-label-container">
                            <input type="radio" id="regular" onClick={()=>handleFareType()} className="radio-element" value="regular" name="fare" checked />
                            <label htmlFor="regular" className="label-element">Regular</label>
                        </div>
                        <div className="radio-label-container">
                            <input type="radio" id="armedforces" onClick={()=>handleFareType()} className="radio-element" value="armedforces" name="fare" />
                            <label htmlFor="armedforces" className="label-element">ArmedForces <span className="new">NEW</span></label>
                        </div>
                        <div className="radio-label-container">
                            <input type="radio" id="student" onClick={()=>handleFareType()} className="radio-element" value="student" name="fare" />
                            <label htmlFor="student" className="label-element">Student</label>
                        </div>
                        <div className="radio-label-container">
                            <input type="radio" id="seniorcitizens" onClick={()=>handleFareType()} className="radio-element" value="seniorcitizen" name="fare" />
                            <label htmlFor="seniorcitizens" className="label-element">Senior Citizen</label>
                        </div>
                        <div className="radio-label-container">
                            <input type="radio" id="doctorsnurses" onClick={()=>handleFareType()} className="radio-element" value="doctorsnurses" name="fare" />
                            <label htmlFor="doctorsnurses" className="label-element">Doctors & Nurses</label>
                        </div>
                        <div className="radio-label-container">
                            <input type="radio" id="doubleseat" onClick={()=>handleFareType()} className="radio-element" value="doubleseat" name="fare" />
                            <label htmlFor="doubleseat" className="label-element">Double Seat</label>
                        </div>
                    </div>
                </div>
            </div>
            <div className="sidebar-flightcards-container">
                <div className="sidebar">
                    <h1 className="popular-filters">Popular Filters</h1>
                    <div className="airline-names">
                    <h1 className="popular-filters airlines">Airlines</h1>
                        {
                            airlinesData.map((airline)=> {
                                const isActive = airline.airlineName === airlineId ? true : false
                                return (
                                    <div key={airline.id}  className="checkbox-label-container"> 
                                        <input type="checkbox" id="checkbox" onClick={()=>handleAirlineId(airline.airlineName)} checked={isActive} className="checkbox" value={airline} />
                                        <label htmlFor="checkbox" className="checkbox-label">{airline.airlineName}</label>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div>
                        <button type="button" className="clear-filters" onClick={handleClearFilters}>Clear Filters</button>
                    </div>
                </div>
                <div className="flights-cards">
                    <h1 className="heading">Flights from {fromCity} to {toCity}</h1>
                    <div className="advisories-guidelines">
                        <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/listing/safety.png" className="mysafety-logo" alt="my safety" />
                        <div>
                            <h1 className="guidelines">Important Advisories & State Guidelines</h1>
                            <p className="guidelines-description">With travel openeing up, govt. advisories and state/UT guidelines are constantly evolving. Please check the latest updates before travelling.<a href="https://www.makemytrip.com/promos/MySafety.html?tab=flights" target="_blank" className="know-more">KNOW MORE</a></p>
                        </div>
                    </div>
                    <div className="sortby-container">
                        <p className="sort-by">SortBy :</p>
                        <p className="departure-arrival">Departure</p>
                        <p className="departure-arrival arrival">Arrival</p>
                        <p className="price">Price <BsArrowUp /></p>
                    </div>
                    {renderFlights()}
                </div>
            </div>
        </div>
    )
}

export default FilterSection;