import React, { Fragment, useState } from "react";
import LoginPage from "../LoginPage/LoginPage";
import SignupPage from "../SignupPage.jsx/SignupPage";
import "./RegistrationPage_Styles.css";

function RegistrationPage(){

  const[isPersonalAccActive,setIsPersonalAccActive]=useState(true);
  const[isBizAccActive,setIsBizAccActive]=useState(false);
  const[signingUp,setSigningUP]=useState(true);
  const [loginORsignup,setLoginORsignup]=useState("Login");
 
  const [admsg,setAddmsg]=useState('Login for great deals and offers');


  function changeRegType(){
    if(signingUp){
    setSigningUP(false);
    setLoginORsignup("Signup");
    }
    else{
      setSigningUP(true);
      setLoginORsignup("Login")
    }
  }
  // function changead() {
  //   if(admsg==="Login for great deals and offers"){
  //     setAddmsg("Book your 1st international trip");
  //   }
  //   else if(admsg==="Book your 1st international trip"){
  //     setAddmsg("Join the club of 10 crore+ happy travellers")
  //   }
  //   else if(admsg==='Join the club of 10 crore+ happy travellers'){
  //     setAddmsg("Login for great deals and offers")
  //   }
  // }
  // setTimeout(()=>{
  //   changead()
  // },1000*3)


  function accTypePersonal(){
    setIsPersonalAccActive(true);
    setIsBizAccActive(false);
  }
  function accTypeBiz() {
    setIsPersonalAccActive(false);
    setIsBizAccActive(true);
    
  }







  return (<Fragment>
    <div className="regcontainer">
      <div className="regCard">
      <div className="account-type-and-ad-box">
        <div className="type-of-acc">
          <button onClick={accTypePersonal} className={isPersonalAccActive ? "btnActive" : "btn" }><h4 className="business-type-text">PERSONAL ACCOUNT</h4></button>
          <button onClick={accTypeBiz} className={isBizAccActive? "btnActive" : "btn" }><h4 className="business-type-text">MYBIZ ACCOUNT</h4></button>
        </div>
          <div className="animateTitle">
          <div className="titleList"></div>
        {/* <h2 className="titleItem">Login for great deals and offers</h2>
        <h2>Book your 1st international trip</h2>
        <h2>Join the club of 10 crore+ happy travellers</h2> */}
        {/* <h2>{admsg}</h2> */}
      </div>
          </div>
            <div className="form-container">

              {signingUp?<SignupPage/>:<LoginPage/>}


              <p className="googleLoginBar">
              <span>Login/Signup</span>
              </p>
                  <div>
                    <button onClick={changeRegType} className="submit-btn"><h3>{loginORsignup}</h3></button>
                  </div>
            </div>
      </div>
    </div>
  </Fragment>)
  
}

export default RegistrationPage;