import Popup from 'reactjs-popup'
import {BsArrowRight} from 'react-icons/bs'
import {BiRupee} from 'react-icons/bi'
import {AiOutlinePlusCircle} from 'react-icons/ai'
import './booking.css'

const Booking = () => {
    return (
        <div className="booking-page">
            <div className='booking-page-header'>
                <h1 className='complete-your-booking'>Complete your booking</h1>
                <div className='menu-items-container'>
                    <p className='menu-items'>Flights Summary</p>
                    <p className='menu-items'>.</p>
                    <p className='menu-items'>Travel Insurance</p>
                    <p className='menu-items'>.</p>
                    <p className='menu-items'>Traveller Details</p>
                    <p className='menu-items'>.</p>
                    <p className='menu-items'>Seats & Meals</p>
                    <p className='menu-items'>.</p>
                    <p className='menu-items'>Add-ons</p>
                </div>
            </div>
            <span className="bgGradient"></span>
            
            <div className='booking-details-container'>
                <div className='booking-details-left-container'>
                    <div className='bookings-container'>
                        <div className="bookings-top-container">
                            <div>
                                <h1 className='from-to-locations'>Delhi<span><BsArrowRight className='arrow-icon' size="16px" /></span>Benglore</h1>
                                <p className='date-styling'>Tuesday,Jan 3</p>
                            </div>
                            <div>
                                <p className='cancellation-fee-apply'>CANCELLATION FEES APPLY</p>
                                <div className='view-fare-rules-container'>
                                    
                                    <Popup trigger={<button className='view-fare-rules'>VIEW FARE RULES</button>} modal>
                                        {close => (
                                            <div className="popup-container">
                                                <button className="close" onClick={close}>
                                                     &times;
                                                </button>
                                            <h1 className="popup-fare-rules">Fare rules</h1>
                                            <div className="popup-cancellation-date-charges">
                                                <button className="charges-button" active>Cancellation charges</button>
                                                <button className="charges-button">Date change charges</button>
                                            </div>
                                            <div className="popup-card">
                                                <div className="popup-airline-logo-locations">
                                                    <img src="https://travelobiz.com/wp-content/uploads/2021/07/Indian-Airlines.jpg" className='popup-airline-logo' alt="airline" />
                                                    <p className='popup-locations'>Delhi - Banglore</p>
                                                </div>
                                                <div className='popup-fare-details'>
                                                    <div className='card'>
                                                        <div className='time-frame-container'>
                                                            <h1 className='time-frame'>Time frame</h1>
                                                            <p className='card-text'>(From Scheduled flight departure)</p>
                                                        </div>
                                                    </div>
                                                    <div className='card'>
                                                        <div className="fee-container">
                                                            <h1 className='time-frame'>Airline Fee + MMT Fee</h1>
                                                            <p className='card-text'>(Per passenger)</p>
                                                        </div>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>0 hours to 2 hours*</p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>ADULT: <span className="span-text">Non Refundable</span></p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>2 hours to 365 days*</p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>ADULT: <span className="span-text"><BiRupee size={15} className="rupee-icon" />1,922 + <BiRupee size={15} className="rupee-icon" />300</span></p>
                                                    </div>
                                                </div>
                                                <p className='card-text star'>*From the Date of Departure</p>
                                            </div>
                                            <div className='popup-bottom'>
                                                <p><span>*Important</span>The airline fee is indicative. MakeMyTrip does not guarantee the accuracy of this information. All fees mentioned are per passenger.</p>
                                            </div>
                                        </div>
                                        )}
                                    </Popup>
                                </div>
                            </div>
                        </div>
                        <div className="bookings-middle-container">
                                <img src="https://travelobiz.com/wp-content/uploads/2021/07/Indian-Airlines.jpg" className='airline-logo' alt="airline" />
                                <p className='airline-name'>Air India</p>
                                <p className='economy'>Economy</p>
                        </div>
                        <div className="bookings-bottom-container">
                            <div className='time-container'>
                                <p className='time'>21:10</p>
                                <p className='time'>00:05</p>
                            </div>
                            <div className='circles-line'>
                                <span className='circle'></span>
                                <span className='line-border'></span>
                                <span className='circle'></span>
                            </div>
                            <div className='locations-container'>
                                <p className="time">Delhi</p>
                                <p className="time">Banglore</p>
                            </div>
                            <div className='baggage-container'>
                                <p className='baggage'>Baggage</p>
                                <p className='baggage-weight'>15KGS</p>
                            </div>
                        </div>
                    </div>
                    <div className='cancellation-container'>
                        <div className="cancellation-top-container">
                            <h1 className='cancellation'>Cancellation Refund Policy</h1>
                                
                            <Popup trigger={<button className='view-policy-button view-policy'>View Policy</button>} modal>
                                        {close => (
                                            <div className="popup-container">
                                                <button className="close" onClick={close}>
                                                     &times;
                                                </button>
                                            <h1 className="popup-fare-rules">Fare rules</h1>
                                            <div className="popup-cancellation-date-charges">
                                                <button className="charges-button" active>Cancellation charges</button>
                                                <button className="charges-button">Date change charges</button>
                                            </div>
                                            <div className="popup-card">
                                                <div className="popup-airline-logo-locations">
                                                    <img src="https://travelobiz.com/wp-content/uploads/2021/07/Indian-Airlines.jpg" className='popup-airline-logo' alt="airline" />
                                                    <p className='popup-locations'>Delhi - Banglore</p>
                                                </div>
                                                <div className='popup-fare-details'>
                                                    <div className='card'>
                                                        <div className='time-frame-container'>
                                                            <h1 className='time-frame'>Time frame</h1>
                                                            <p className='card-text'>(From Scheduled flight departure)</p>
                                                        </div>
                                                    </div>
                                                    <div className='card'>
                                                        <div className="fee-container">
                                                            <h1 className='time-frame'>Airline Fee + MMT Fee</h1>
                                                            <p className='card-text'>(Per passenger)</p>
                                                        </div>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>0 hours to 2 hours*</p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>ADULT: <span className="span-text">Non Refundable</span></p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>2 hours to 365 days*</p>
                                                    </div>
                                                    <div className='card'>
                                                        <p className='card-text'>ADULT: <span className="span-text"><BiRupee size={15} className="rupee-icon" />1,922 + <BiRupee size={15} className="rupee-icon" />300</span></p>
                                                    </div>
                                                </div>
                                                <p className='card-text star'>*From the Date of Departure</p>
                                            </div>
                                            <div className='popup-bottom'>
                                                <p><span>*Important</span>The airline fee is indicative. MakeMyTrip does not guarantee the accuracy of this information. All fees mentioned are per passenger.</p>
                                            </div>
                                        </div>
                                        )}
                                    </Popup>     
                        </div>
                        <div className="cancellation-middle-container">
                            <img src="https://travelobiz.com/wp-content/uploads/2021/07/Indian-Airlines.jpg" className='airline-logo' alt="airline" />
                            <p className='airline-name'>Delhi-Banglore</p>
                        </div>
                        <div className='cancellation-penality-container'>
                            <div className='cancellation-penality'>
                                <h1 className='cancellation-penality-text'>Cancellation Penality :</h1>
                                <h1 className='cancellation-penality-text'>Cancel Between (IST) :</h1>
                            </div>
                            <div className='cancellation-penality-duraion'>
                                <div className='price-container'>
                                    <p className='price'><BiRupee />3,800</p>
                                    <p className='price'><BiRupee />10,320</p>
                                </div>
                                <div className='colored-line'></div>
                                <div className='dates-container'>
                                    <h1 className='date-text'>Now</h1>
                                    <div>
                                        <h1 className='date-text'>3 Jan</h1>
                                        <p className='cancellation-penality-text penality-time'>18:30</p>
                                    </div>
                                    <div>
                                        <h1 className='date-text'>3 Jan</h1>
                                        <p className='cancellation-penality-text penality-time'>20:30</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="cancellation-bottom-container">
                            <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/rta_assets/fare-upgrade.png" className='cancellation-logo' alt="cancellation" />
                            <h1 className='more-fare'>More fares with flexible refund & date change policy available!</h1>
                            <button className='upgrade-button upgrade'>UPGRADE</button>
                        </div>
                    </div>
                    <div className='unsure-of-your-travel-plans'>
                        <h1 className='unsure-your-travel-plans-text'>Unsure of your travel plans?</h1>
                        <div className='add-free-date-change-card'>
                            <div className='add-free-card'>
                                <input type="checkbox" className='checkbox' />
                            </div>
                            <div className='add-free-card middle'>
                                <p className='add-free-text'>Add Free Date Change</p>
                                <p className='add-free-description'><span>Save up to <BiRupee className='rupee-icon' />3,250</span> on date change charges up to 2 hours before departure. You just pay the fare difference.</p>
                                <a href="https://promos.makemytrip.com/digit-tnc-dom/TermsAndConditions.html" target="_blank">View T&C</a>
                            </div>
                            <div className='add-free-card bottom'>
                                <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/rta_assets/travel-plan-fdc-card.png" className='calender-logo' />
                                <p className='travel-plans-price'><BiRupee />207</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='booking-details-right-container'>
                    <div className='fare-summary-card'>
                        <h1 className='fare-summary'>Fare Summary</h1>
                        <div className='base-fare-card'>
                            <div>
                                <AiOutlinePlusCircle className='circle-icon' />
                                <span className='summary-text'>Base Fare</span> 
                            </div>   
                            <p className='price'><BiRupee />1,922</p>
                        </div>
                        <div className='base-fare-card'>
                            <div>
                                <AiOutlinePlusCircle className='circle-icon' />
                                <span className='summary-text'>Fee & Surcharges</span>
                            </div>
                            <p className='price'><BiRupee />580</p>
                        </div>
                        <div className='base-fare-card bottom-fare-summary-card'>
                            <div>
                                <AiOutlinePlusCircle className='circle-icon' />
                                <span className='summary-text'>Other Services</span>
                            </div>
                            <p className='price'><BiRupee />10</p>
                        </div>
                        <div className='total-amount-container'>
                            <h1 className='total-amount'>Total Amount</h1>
                            <p className='total-amount'><BiRupee />2,512</p>
                        </div>
                    </div>
                    <div className='second-container'>
                        <div className='promocodes-card'>
                            <h1 className='promo-text'>PROMO<span className='codes'>CODES</span></h1>
                            <div className='promo-code-logo-container'>
                                <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/rta_assets/promo-code.png" className='promocode-logo' alt="promocode" />
                            </div>
                        </div>
                        <input type="search" className='input-element' placeholder='Enter promo code here' />
                        <div className='promo-card-one'>
                            <input type="radio" className='radio-element' />
                            <div className='promo-middle-card'>
                                <h1 className='promo-code'>MMTSUPER</h1>
                                <p className='promotext'>Use this coupon and get Rs 250 instant discount on your flight booking.</p>
                                <a href="https://www.makemytrip.com/promos/df-mmtsuper-amazon-22042022.html" target="_blank" className='anchor-element'>Terms & Conditions</a>
                            </div>
                            <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/review/INSTANT.png?v=1" className='promocodelogo' alt="promo" />
                        </div>
                        <div className='promo-card-one card-two'>
                            <input type="radio" className='radio-element' />
                            <div className='promo-middle-card'>
                                <h1 className='promo-code'>MMTAU</h1>
                                <p className='promotext'>Use this coupon and get Rs 250 instant discount on your flight booking.</p>
                                <a href="https://www.makemytrip.com/promos/df-mmtsuper-amazon-22042022.html" target="_blank" className='anchor-element'>Terms & Conditions</a>
                            </div>
                            <img src="https://imgak.mmtcdn.com/flights/assets/media/dt/review/IC.png?v=1" className='promocodelogo' alt="promo" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Booking;